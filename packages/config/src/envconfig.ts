import type * as BrowserOrNode from 'browser-or-node';
import dotenv from 'dotenv';
import fromEntries from 'object.fromentries';
const engineType: typeof BrowserOrNode & { isReactNative: boolean | undefined } =
    typeof navigator !== 'undefined' && navigator.product === 'ReactNative'
        ? { isReactNative: true }
        : require('browser-or-node');

const allConf = {};
const env = typeof process !== 'undefined' ? process.env ?? {} : {};
const isProd = env.NODE_ENV === 'production';

export function envConfig<TEnv extends Record<string, string | boolean | number | bigint | undefined | null>, TCustom>(
    envConf: TEnv,
    customConf?: TCustom,
    dotEnvConf?: dotenv.DotenvConfigOptions,
): TEnv & TCustom & { NODE_ENV?: 'production' | 'development'; isDev: boolean; isProd: boolean } & typeof engineType {
    dotenv && dotenv.config && dotenv.config(dotEnvConf);
    for (const name in envConf) {
        if (name in env) {
            let val: any = envConf[name];
            const envVal = env[name];
            switch (typeof val) {
                case 'number':
                    const num = +(envVal || NaN);
                    if (isFinite(num)) val = num;
                    break;
                case 'bigint':
                    try {
                        val = BigInt(envVal!);
                    } catch {}
                    break;
                case 'boolean':
                    val = envVal === 'true' || (envVal !== undefined && +envVal !== 0);
                    break;
                default:
                    val = envVal;
                    break;
            }
            envConf[name] = val as any;
        }
    }
    Object.assign(allConf, envConf);
    return Object.assign(envConf, customConf, {
        NODE_ENV: env.NODE_ENV,
        isDev: !isProd,
        isProd,
        ...engineType,
    }) as any;
}

export namespace envConfig {
    export function getAll(masked?: 'masked' | 'maskedInProd') {
        if (masked === 'masked' || (masked === 'maskedInProd' && isProd)) {
            return fromEntries(
                Object.entries(allConf).map(([key, val]) => {
                    if (val !== null && val !== undefined) {
                        val =
                            key.startsWith('PRIVATE_') || key.endsWith('_PRIVATE')
                                ? `*** (${(val as any).toString().length})`
                                : val;
                    }
                    return [key, val];
                }),
            );
        } else {
            return allConf;
        }
    }
}

import { plainToClass } from 'class-transformer';
import { array, object, string } from '../src';

class Item {
    @string({ optional: true })
    optStr?: string;

    @string()
    str: string;
}

class Container {
    @string({ optional: true })
    _id?: string;

    @array(object(Item, { each: true }))
    items: Item[];
}

test('TestModel1', () => {
    const c1 = plainToClass(Container, {
        items: [
            {
                str: 'foobar',
            },
        ],
    });
    console.log(c1);

    expect(c1).toMatchObject({});
});

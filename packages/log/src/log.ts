//@ts-ignore
import { envConfig } from '@uk/config';
import stringify from 'fast-safe-stringify';
import * as path from 'path';
import { StopWatch } from './stopwatch';
import { TryPayload } from './trypayload';

const instances = {} as {
    [tag: string]: number;
};

const conf = envConfig({});

export type ErrorType = unknown;

export class Log {
    readonly tag: string;
    instance: number;

    constructor(tag: string | Log) {
        this.tag = typeof tag === 'string' ? tag : tag.tag;
        this.tag =
            this.tag.endsWith('.ts') || this.tag.endsWith('.js')
                ? this.tag
                      .substring(this.tag.lastIndexOf('/') + 1)
                      .slice(0, -3)
                      .toUpperCase()
                : this.tag;
        this.instance = instances[this.tag] || 0;
        instances[this.tag] = this.instance + 1;
    }

    indexed(): object | undefined;
    indexed(set: object): this;
    indexed(set?: object): this | object | undefined {
        if (set === undefined) return this.#index;
        this.#index = set;
        return this;
    }

    trace(msg: string, unindexed?: object, indexed?: object) {
        this.append('TRACE', msg, unindexed, indexed);
    }

    debug(msg: string, unindexed?: object, indexed?: object) {
        this.append('DEBUG', msg, unindexed, indexed);
    }

    info(msg: string, unindexed?: object, indexed?: object) {
        this.append('INFO', msg, unindexed, indexed);
    }

    warn(msgOrError: ErrorType | string, unindexed?: object, indexed?: object): void;
    warn(msgWithError: [string, ErrorType], unindexed?: object, indexed?: object): void;
    warn(msg: any, unindexed?: object, indexed?: object) {
        this.append('WARN', msg, unindexed, indexed);
    }

    error(msgWithError: [string, ErrorType], unindexed?: object, indexed?: object): void;
    error(msgOrError: ErrorType | string, unindexed?: object, indexed?: object): void;
    error(msg: any, unindexed?: object, indexed?: object): void {
        this.append('ERROR', msg, unindexed, indexed);
    }

    fatal(msgWithError: [string, ErrorType], unindexed?: object, indexed?: object): void;
    fatal(msgOrError: ErrorType | string, unindexed?: object, indexed?: object): void;
    fatal(msg: any, unindexed?: object, indexed?: object): void {
        this.append('FATAL', msg, unindexed, indexed);
    }

    here(msg?: string, unindexed?: object, indexed?: object) {
        const err = new Error();
        let rm = '';
        if (path && require.main?.filename) {
            rm = path.dirname(require.main.filename);
        }
        msg = `Log.here() ${err.stack?.split('\n')[2].trim().replace(rm, '')}`;
        this.append('TRACE', msg, unindexed, indexed);
    }

    append(level: Log.Level, msgWithError: [string | undefined, ErrorType], unindexed?: object, indexed?: object): void;
    append(level: Log.Level, msgOrError: string, unindexed?: object, indexed?: object): void;
    append(level: Log.Level, msg: any, unindexed?: object, indexed?: object) {
        const d = Date.now();

        let m;
        let error: Error | undefined;

        if (msg) {
            if (Array.isArray(msg)) {
                m ??= msg[0]?.toString();
                error = msg[1] ?? new Error('Undefined error');
            } else if (typeof msg === 'string') {
                m = msg;
            } else {
                error = msg;
            }
        }
        let e;
        if (error) {
            if (error instanceof Error) e = error;
            else if (typeof error === 'string') e = new Error(error);
            else if (typeof error === 'object' && error && 'message' in error && 'stack' in error && 'name' in error) {
                e = error;
            } else {
                const c = (error as any).toString();
                if (c) e = new Error(c);
            }
        }
        if (!m) {
            m = e?.message ?? 'WARNING: Empty log message';
        }
        Log.push({
            d,
            l: level,
            m,
            e,
            u: unindexed,
            p: this.#index ? { ...this.#index, ...indexed } : indexed,
            t: this.tag,
            i: this.instance,
        });
    }

    try<T>(message: string | [Log.Level, string] | Log.TryOpts, func: (payload: TryPayload) => T): T {
        let opts: Required<Log.TryOpts> = {} as any;
        if (typeof message === 'string') {
            opts.errMsg = opts.msg = message;
        } else if (Array.isArray(message)) {
            opts.errMsg = opts.msg = message[1];
            opts.lvl = message[0];
        } else {
            opts = message as any;
        }
        opts.errLvl = opts.errLvl || 'ERROR';
        opts.errMsg = opts.errMsg || opts.msg;
        opts.lvl = opts.lvl || 'INFO';

        const start = Log.now();
        const payload = new TryPayload();
        let rv;

        try {
            rv = func(payload);
        } catch (err) {
            this.append(opts.errLvl, [opts.errMsg, err], payload['uError'], {
                execTime: Log.timeSpan(start),
                ...payload['iError'],
            });
            throw err;
        }
        const execTime = Log.timeSpan(start);
        if (rv instanceof Promise) {
            rv = rv.then(
                val => {
                    this.append(opts.lvl, opts.msg, payload['uSuccess'], {
                        execTime,
                        asyncTime: Log.timeSpan(start),
                        ...payload['iSuccess'],
                    });
                    return val;
                },
                err => {
                    this.append(opts.errLvl, [opts.msg, err], payload['uError'], {
                        execTime,
                        asyncTime: Log.timeSpan(start),
                        ...payload['iError'],
                    });
                    throw err;
                },
            );
        } else {
            this.append(opts.lvl, opts.msg, payload['uSuccess'], {
                ...payload['iSuccess'],
                execTime,
            });
        }
        return rv as T;
    }

    get catch() {
        return (err: unknown, level: Log.Level = 'ERROR', msg?: string, unindexed?: object, indexed?: object) => {
            this.append(level, [msg, err], unindexed, indexed);
        };
    }

    get hcf() {
        return (err: unknown, msg?: string, unindexed?: object, indexed?: object) => {
            this.append('FATAL', [msg, err], unindexed, indexed);
            process.exit(1);
        };
    }

    stopWatch(stopped?: boolean): StopWatch;
    stopWatch(label: string): StopWatch;
    stopWatch<R>(func: () => R): [R, StopWatch];
    stopWatch<R>(label: string, func: () => R): [R, StopWatch];
    stopWatch(lblOrFuncOrStop: any, fun?: () => any): any {
        const t = typeof lblOrFuncOrStop;
        const lbl = t === 'string' ? lblOrFuncOrStop : undefined;
        let sw;
        if (lbl) {
            this.#stopwatches ??= new Map();
            sw = this.#stopwatches.get(lbl);
            if (!sw) {
                this.#stopwatches.set(lbl, (sw = new StopWatch(this, lbl)));
            }
        } else {
            sw = new StopWatch(this, lbl);
        }
        const f = fun ?? (t === 'function' ? lblOrFuncOrStop : undefined);
        if (t !== 'boolean' || lblOrFuncOrStop === false) sw.start();
        if (f) {
            const rv = f();
            return [rv, sw];
        } else {
            return sw;
        }
    }

    #stopwatches?: Map<string, StopWatch>;

    watchTime<R>(label: string): Log.WatchTimer;
    watchTime<R>(label: string, func: () => R): R;
    watchTime<R>(label: string, func?: () => R): R | Log.WatchTimer {
        if (!this.#watchTimers) this.#watchTimers = {};
        let timer = this.#watchTimers[label];
        if (!func) return timer ? { ...timer, avg: timer.time / timer.count } : { count: 0, time: 0, avg: 0 };
        if (!timer) timer = this.#watchTimers[label] = { count: 0, time: 0, avg: 0 };
        const startAt = Log.now();
        const rv = func();
        timer.time += Log.timeSpan(startAt);
        timer.count++;
        return rv;
    }

    httpMiddleware(options?: Log.HttpLogOptions) {
        const prod = process.env.NODE_ENV === 'production';
        const opts = options || {
            level: 'INFO',
            body: !prod,
            params: !prod,
        };
        opts.level = opts.level || 'INFO';
        return (req: any, resp: any, next: () => void) => {
            const startedAt = (req.startedAt = Log.now());
            resp.on('finish', () => {
                const status = resp.statusCode;
                let level = opts.level || 'DEBUG';
                if (status >= 500) {
                    level = 'ERROR';
                } else if (status >= 400) {
                    level = 'WARN';
                }
                if (opts.ignore && opts.ignore.exec(req.originalUrl) && level < 'WARN') return;
                this.append(level, `${req.method} ${req.originalUrl} => ${status}`, {
                    time: Log.timeSpan(startedAt),
                    from: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                    fresh: opts.fresh ? req.fresh : undefined,
                    body: opts.body ? req.body : undefined,
                    cookies: opts.cookies ? req.cookies : undefined,
                    params: opts.params ? req.params : undefined,
                    xhr: opts.xhr ? req.xhr : undefined,
                });
            });
            next();
        };
    }

    #index?: object;
    #watchTimers?: Record<string, Log.WatchTimer>;
}

export namespace Log {
    export interface TryOpts {
        lvl?: Level;
        msg: string;
        errLvl?: Level;
        errMsg?: string;
    }

    export interface WatchTimer {
        count: number;
        time: number;
        avg: number;
    }

    export interface Item {
        d: number; // Event time, Unix ms
        t: string; // Tag
        i: number; // Instance
        l: Level; // Level
        m: string; // Message
        e?: Error; // Error
        p?: object; // Indexed payload
        u?: object; // Unindexed payload
    }

    export type Level = 'TRACE' | 'DEBUG' | 'INFO' | 'WARN' | 'ERROR' | 'FATAL';

    export interface HttpLogOptions {
        level?: Level;
        body?: boolean;
        cookies?: boolean;
        fresh?: boolean;
        params?: boolean;
        xhr?: boolean;
        ignore?: RegExp;
    }

    export type Logger = JsonLogger | ItemLogger;
    export interface ItemLogger {
        appendItem(item: Item): void;
    }
    export interface JsonLogger {
        appendJson(item: string): void;
    }

    export type Hook = (item: Item) => void;

    export let loggers: { logger: Logger; filter?: (item: Item) => boolean }[] = [];

    export function push(item: Item) {
        if (item.e) {
            item.e = {
                ...item.e,
                message: item.e.message,
                name: item.e.name,
                stack: item.e.stack ? item.e.stack.toString() : 'No stacktrace',
            };
        }
        let json;
        hook?.(item);
        for (const { logger, filter } of loggers) {
            if (filter && !filter(item)) continue;
            if ('appendJson' in logger) {
                if (!json) json = stringify(item);
                logger.appendJson(json);
            } else {
                logger.appendItem(item);
            }
        }
    }

    let hook: Hook | undefined;
    export function setHook(newHook: Hook | undefined) {
        hook = newHook;
    }

    export function timeSpan(from: number, to?: number) {
        if (!to) to = now();
        return Math.abs(Math.round((to - from) * 1000) / 1000);
    }

    export function sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    export function now() {
        if (conf.isNode || (conf.isDeno && typeof process.hrtime === 'function')) {
            const hr = process.hrtime();
            return hr[0] * 1000 + hr[1] / 1000000;
        } else {
            return Date.now();
        }
    }

    export class Dummy extends Log {
        constructor() {
            super('');
        }

        indexed(): object | undefined;
        indexed(set: object): this;
        indexed(set?: object): this | object | undefined {
            return {};
        }

        trace() {}

        debug() {}

        info() {}

        warn() {}

        error(): void {}

        fatal(): void {}

        append() {}

        try<T>(_message: string | [Log.Level, string] | Log.TryOpts, func: (payload: TryPayload) => T): T {
            const rv = func(new TryPayload());
            if (rv instanceof Promise) {
                return rv.finally(() => {}) as any;
            } else {
                return rv;
            }
        }

        now() {
            return Log.now();
        }

        watchTime<R>(label: string): Log.WatchTimer;
        watchTime<R>(label: string, func: () => R): R;
        watchTime<R>(label: string, func?: () => R): R | Log.WatchTimer {
            return Dummy.zeroWatchResult;
        }

        private static zeroWatchResult = { count: 0, avg: 0, time: 0 };

        httpMiddleware(options?: Log.HttpLogOptions) {
            return (_req: any, _resp: any, next: Function) => next();
        }
    }

    const dummyLog = new Dummy();
    export function dummy() {
        return dummyLog;
    }
}
